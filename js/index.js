//fix sinh id khi xoa kieu gi cung trùng
//khi tạo thi gắn value để biết cha ntn
$().ready(function () {

    let blockSortable = $('ol.sortable');

    blockSortable.on('change', 'input', function () {
        let data = $(this).val();
        $(this).attr('value', data);
    });
    blockSortable.on('click', '.delete-menu', function () {
        let id = $(this).attr('data-id');
        $('#menu_' + id).remove();
    });

    blockSortable.on('click', '.disclose', function () {
        // chọn nó đên cha chứa nó, tiếp cha chứ nó. rồi check có tag ol li không
        if ($(this).parent().parent().has('ol li').length < 0)
            $(this).closest('li').removeClass("mjs-nestedSortable-expanded");
            $(this).closest('li').toggleClass('mjs-nestedSortable-collapsed').toggleClass('mjs-nestedSortable-expanded');
    });

    blockSortable.nestedSortable({
        forcePlaceholderSize: true,
        handle: 'div',
        helper: 'clone',
        items: 'li',
        opacity: .6,
        placeholder: 'placeholder',
        revert: 250,
        tabSize: 25,
        tolerance: 'pointer',
        toleranceElement: '> div',
        maxLevels: 2,
        isTree: true,
        expandOnHover: 700,
        startCollapsed: true,
        excludeRoot: true,
        rootID: "root",
    });

    $('#toArray').on('click',function () {
        let array = [];
        let sortable = $("ol.sortable").nestedSortable('toArray', {startDepthCount: 0});
        sortable.forEach(function (entry) {
            let object = $('#menu_' + entry.item_id);
            let parentID = entry.parent_id;
            let id = object.find("input[name='id']").first().attr("value");
            let name = object.find("input[name='name']").first().attr("value");
            let url = object.find("input[name='url']").first().attr("value");
            let icon = object.find("input[name='icon']").first().attr("value");
            let parent = '';
            if (parentID !== null) {
                let objectParent = $('#menu_' + parentID);
                parent = objectParent.find("input[name='id']").first().attr("value");
            }
            array.push({id: id, parent: parent, name: name, icon: icon, url: url});
        });
        alert(JSON.stringify(array, null, 10));
    });

    $('#addMenu').on('click', function () {
        let countMenu = $("ol.sortable li").length;
        let idMenuNew = countMenu + 1;
        let menu = `<li style="display: list-item;" id="menu_${idMenuNew}">
                        <div class="menu-div ui-sortable-handle">
                            <span class="disclose">&nbsp;</span>
                            <span data-id="${idMenuNew}" class="item-title">
                                <input title="" name="id" value="" hidden/>
                                <input title="" name="name" value=""/>
                                <input title="" name="url" value=""/>
                                <input title="" name="icon" value=""/>
                            </span>
                            <span data-id="${idMenuNew}" class="delete-menu">&nbsp;</span>
                        </div>
                    </li>`;
        $('ol.sortable').append(menu);
    });
});